# Introduction
This project contains a small application to generate random passwords.
# Compilation
To compile ```passgen``` clone the repository to your local machine and just run the ```compile.sh``` script located in the root directory of the project.
# Usage
## Fist setup
To setup passgen just compile the application as per Compilation and copy the application into ```/bin/.```.
## Commands
To use the application just type the command ```passgen [parameter] [length] [optional]```.
Valid parameters include:
1.  ```-h``` Prints the help dialog
2.  ```-a``` Generates a password using all characters available
3.  ```-l``` Generates a password using only lowercase letters aswell as symbols
4.  ```-L``` Generates a password using only lowercase letters
5.  ```-c``` Generates a password using only uppercase letters aswell as symbols
6.  ```-C``` Generates a password using only uppercase letters
7.  ```-s``` Generates a password using only symbols
8.  ```-S``` Generates a password using only letters
9.  ```-n``` Generates a password using only numbers and letters
10. ```-N``` Generates a password using only numbers
11. ```-M``` Generates a password using only numbers and symbols
12. ```-U``` Generates a password based on a set of characters provided by the user as an ```optional``` argument
13. ```-G``` Prints the available characters to the console
I have added the option to generate a strong password using the ```strong``` paramter. It will generate a 64 character password using all the available characters. In other words its a short cut for ```passgen -a 64```.
## Example
If we run the following command:<br>
```passgen -a 10```<br>
We will get a ten character long password using all possible characters, which may look like:<br>
```Kb&V3maJFc```
