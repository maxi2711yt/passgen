#!/bin/bash

echo '-h'
./passgen -h

echo '-a'
./passgen -a 100

echo '-l'
./passgen -l 100

echo '-L'
./passgen -L 100

echo '-c'
./passgen -c 100

echo '-C'
./passgen -C 100

echo '-s'
./passgen -s 100

echo '-S'
./passgen -S 100

echo -n '-'
echo 'n'
./passgen -n 100

echo '-N'
./passgen -N 100

echo '-M'
./passgen -M 100

echo '-U'
./passgen -U 100 "MaxIst"

echo '-G'
./passgen -G 100
