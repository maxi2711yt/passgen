#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "../include/defines.h"
#include "../include/help.h"

#include <string.h>

void genPass(const char option, const int count, const size_t length, int argc, char **argv)
{
    char password[length + 1];
    password[length] = '\0';

    switch (option)
    {
    case 'a': {
        char symbols[2 * ALPHABET_SIZE + SYMBOL_SIZE + NUMBER_SIZE + 1]; // Creating a symbol buffer
        symbols[2 * ALPHABET_SIZE + SYMBOL_SIZE + NUMBER_SIZE] = '\0';

        for (size_t i = 0; i < ALPHABET_SIZE; i++) // Filling the symbol buffer with the lowercase und uppercase letters
        {
            symbols[i] = ALPHABET_UPPERCASE[i];
            symbols[i + ALPHABET_SIZE] = ALPHABET_LOWERCASE[i];
        }

        for (size_t i = 0; i < SYMBOL_SIZE; i++) // Filling the symbol buffer with the symbols
        {
            symbols[i + ALPHABET_SIZE * 2] = SYMBOLS[i];
        }

        for(size_t i = 0; i < NUMBER_SIZE; i++) // Filling the symbol buffer with numbers
        {
            symbols[i + ALPHABET_SIZE * 2 + SYMBOL_SIZE] = NUMBERS[i];
        }

        for (size_t i = 0; i < length; i++) // generating the password
        {
            password[i] = symbols[rand() % (ALPHABET_SIZE * 2 + SYMBOL_SIZE + NUMBER_SIZE)];
        }
        break;
    }
    case 'l': {
        char symbols[ALPHABET_SIZE + SYMBOL_SIZE + 1]; // Creating the symbol buffer
        symbols[ALPHABET_SIZE + SYMBOL_SIZE] = '\0';

        for (size_t i = 0; i < ALPHABET_SIZE; i++) // Filling the symbol buffer with lowercase letters
        {
            symbols[i] = ALPHABET_LOWERCASE[i];
        }

        for (size_t i = 0; i < SYMBOL_SIZE; i++) // Filling the symbol buffer with symbols
        {
            symbols[i + ALPHABET_SIZE] = SYMBOLS[i];
        }

        for (size_t i = 0; i < length; i++) // Generating the password
        {
            password[i] = symbols[rand() % (ALPHABET_SIZE + SYMBOL_SIZE)];
        }
        break;
    }
    case 'L': {
        for (size_t i = 0; i < length; i++) // Generating the password for only lowercase letters. This action doesn't require generating a char[] for the symbols as it is already defined in ../include/defines.h
        {
            password[i] = ALPHABET_LOWERCASE[rand() % (ALPHABET_SIZE)];
        }
        break;
    }
    case 'c': {
        char symbols[ALPHABET_SIZE + SYMBOL_SIZE + 1]; // Creating the symbol buffer
        symbols[ALPHABET_SIZE + SYMBOL_SIZE] = '\0';

        for (size_t i = 0; i < ALPHABET_SIZE; i++) // Filling the symbol buffer with uppercase letters
        {
            symbols[i] = ALPHABET_UPPERCASE[i];
        }

        for (size_t i = 0; i < SYMBOL_SIZE; i++) // Filling the symbol buffer with symbols
        {
            symbols[i + ALPHABET_SIZE] = SYMBOLS[i];
        }
 
        for (size_t i = 0; i < length; i++) // Generating the password
        {
            password[i] = symbols[rand() % (ALPHABET_SIZE + SYMBOL_SIZE)];
        }
        break;
    }
    case 'C': {
        for (size_t i = 0; i < length; i++) // Generating the password for only uppercase letters. This action doesn't require generating a char[] for the symbols as it is already defined in ../include/defines.h
        {
            password[i] = ALPHABET_UPPERCASE[rand() % (ALPHABET_SIZE)];
        }
        break;
    }
    case 's': {
        for (size_t i = 0; i < length; i++) // Generating the password for only symbols. This action doesn't require generating a char[] for the symbols as it is already defined in ../include/defines.h 
        {
            password[i] = SYMBOLS[rand() % (SYMBOL_SIZE)];
        }
        break;
    }
    case 'S': {
        char symbols[ALPHABET_SIZE * 2 + 1]; // Creating symbol buffer
        symbols[ALPHABET_SIZE * 2] = '\0';

        for (size_t i = 0; i < ALPHABET_SIZE; i++) // Filling the symbol buffer with uppercase and lowercase letters
        {
            symbols[i] = ALPHABET_UPPERCASE[i];
            symbols[i + ALPHABET_SIZE] = ALPHABET_LOWERCASE[i];
        }

        for (size_t i = 0; i < length; i++) // Generating the password
        {
            password[i] = symbols[rand() % (ALPHABET_SIZE * 2)];
        }
        break;
    }
    case 'U': {
        if (argc < 4) // Checking if the right amount of arguments have been provided.
        {
            printf("Insufficent number of arguments provided!\n");
            help();
            exit(1);
        }
        char *symbols = argv[3]; // Extracting the custom character set out of the argvs
        size_t symbolscount = strlen(symbols); // Calculating the amount of characters given by the user

        for (size_t i = 0; i < length; i++) // Generating the password
        {
            password[i] = symbols[rand() % symbolscount];
        }
        break;
    }
    case 'n': {
        char symbols[ALPHABET_SIZE * 2 + NUMBER_SIZE + 1]; // Creating the symbol buffer
        symbols[ALPHABET_SIZE * 2 + NUMBER_SIZE] = '\0'; 

        for (size_t i = 0; i < ALPHABET_SIZE; i++) // Filling the symbol buffer with lowercase and uppercase letters
        {
            symbols[i] = ALPHABET_UPPERCASE[i];
            symbols[i + ALPHABET_SIZE] = ALPHABET_LOWERCASE[i];
        }

        for (size_t i = 0; i < NUMBER_SIZE; i++) // Filling the symbol buffer with numbers
        {
            symbols[i + 2 * ALPHABET_SIZE] = NUMBERS[i];
        }

        for (size_t i = 0; i < length; i++) // Generating password
        {
            password[i] = symbols[rand() % (ALPHABET_SIZE * 2 + NUMBER_SIZE)];
        }
        break;
    case 'N': {
        for (size_t i = 0; i < length; i++) // Generating password for only numbers. This action doesn't require the creation of a char[] as it is already defined in ../include/defines.h
        {
            password[i] = NUMBERS[rand() % NUMBER_SIZE];
        }
        break;
    }
    case 'M': {
        char symbols[NUMBER_SIZE + SYMBOL_SIZE + 1]; // Creating the symbol buffer
        symbols[NUMBER_SIZE + SYMBOL_SIZE] = '\0';

        for (size_t i = 0; i < NUMBER_SIZE; i++) // Filling the symbol buffer with numbers
        {
            symbols[i] = NUMBERS[i];
        }

        for (size_t i = 0; i < SYMBOL_SIZE; i++) // Filling the symbol buffer with symbols
        {
            symbols[i + NUMBER_SIZE] = SYMBOLS[i];
        }

        for (size_t i = 0; i < length; i++) // Generating the password
        {
            password[i] = symbols[rand() % (NUMBER_SIZE + SYMBOL_SIZE)];
        }
        break;
    }
    }
    default: // As default the application will quit and print out the unknown option to the user.
        printf("Unknown option %c.\n", argv[1][1]);
        help();
        exit(1);
    }
    printf("%s\n", password); // Printing the generated password to the console

    if (count == 1) // Returning if count == 1
        return;
    else
        genPass(option, count - 1, length, argc, argv); // Recursively calling genPass to generate the number of passwords in count 
}

int main(int argc, char **argv)
{
    // Seeding rand() with the current time
    time_t seed;
    time(&seed);

    srand(seed);
    
    // Checking if argv[1] exists
    if (argc < 2)
    {
        printf("Not enough arguments have been passed!\n");
        help();
        exit(1);
    }

    // Chekcking if argv[1] is atleast 2 characters long
    if (strlen(argv[1]) < 2)
    {
        printf("Invalid option \"%s\" provided!\n", argv[1]);
        help();
        exit(1);
    }

    // Shortcut for -a 64
    if(strcmp("strong", argv[1]) == 0)
    {
        genPass('a', 1, 64, argc, argv);
        return 0;
    }

    // help parameter gets treated seperatly
    if (argv[1][1] == 'h')
    {
        help();
        return 0;
    }

    // G parameter gets handeled outside of the passGen function
    if (argv[1][1] == 'G')
    {
        char characters[ALPHABET_SIZE * 2 + SYMBOL_SIZE + 1];
        characters[ALPHABET_SIZE * 2 + SYMBOL_SIZE] = '\0';

        for (size_t i = 0; i < ALPHABET_SIZE; i++)
        {
            characters[i] = ALPHABET_LOWERCASE[i];
            characters[i + ALPHABET_SIZE] = ALPHABET_UPPERCASE[i];
        }

        for (size_t i = 0; i < SYMBOL_SIZE; i++)
        {
            characters[i + ALPHABET_SIZE * 2] = SYMBOLS[i];
        }

        printf("%s\n", characters);
        return 0;
    }

    // Checking if there are enough parameters for the other possible options
    if (argc < 3)
    {
        printf("Not enough arguments have been passed!\n");
        help();
        exit(1);
    }

    // Extracting the passwords length out of the argvs
    size_t length = atoi(argv[2]);

    if (length == 0)
    {
        printf("Please provide a valid length greater than 0!\n");
        help();
        exit(1);
    }

    // Checking how many passwords should be generated
    size_t count = 1;
    if (argc == 5 && argv[1][1] == 'U')
    {
        count = atoi(argv[4]);
    }
    else if (argc > 3 && argv[1][1] != 'U')
    {
        count = atoi(argv[3]);
    }

    if (count == 0)
    {
        printf("Invalid number count of passwords provided!\n");
        help();
        exit(1);
    }

    genPass(argv[1][1], count, length, argc, argv);
    return 0;
}
