#include "../include/help.h"

void help()
{
    printf("Usage:\n");
    printf("passgen [parameter] [length] [optional] [count=1]\n");
    printf("Parameters:\n");
    printf("\t-h\tPrints the help dialog.\n");
    printf("\t-a\tCreates a password using all possible characters.\n");
    printf("\t-l\tCreates a password using only lowercase letters and symbols.\n");
    printf("\t-L\tCreates a password using only lowercase letters.\n");
    printf("\t-c\tCreates a password using only uppercase letters and symbols.\n");
    printf("\t-C\tCreates a password using only uppercase letters.\n");
    printf("\t-s\tCreates a password using only symbols.\n");
    printf("\t-S\tCreates a password using only letters.\n");
    printf("\t-n\tCreates a password using only letters and numbers.\n");
    printf("\t-N\tCreates a password using only numbers.\n");
    printf("\t-M\tCreates a password using only numbers and symbols.\n");
    printf("\t-U\tCreates a password using a user defined set of characters provided as optional.\n");
    printf("\t-G\tPrints the available characters to the console.\n");
    printf("\tstrong\tShortcut for \'passgen -a 64\'\n");
    printf("Count refers to the number of passwords to be generated. Count is set to 1 by default.\n");
}
