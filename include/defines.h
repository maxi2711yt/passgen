#pragma once

#define ALPHABET_UPPERCASE "ABCDEFGHIJKLMNOPQRTSUVWXYZ" // Defines the uppercase of the alphabet
#define ALPHABET_LOWERCASE "abcdefghijklmnopqrtsuvwxyz" // Defines the lowercase of the alphabet
#define ALPHABET_SIZE 26                                // Stores the amount of letters in the alphabet

#define SYMBOLS "#$!@^&*~.,/\\[]{}()\"\';:<>?-+="       // Stores all symbols used
#define SYMBOL_SIZE 28                                  // Stoes the amount of symbols used

#define NUMBERS "0123456789"                            // Stores all the numbers used
#define NUMBER_SIZE 10                                  // Stores the amount of numbers used
